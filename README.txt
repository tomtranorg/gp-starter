Requirements
------------
In addition to the standard Drupal requirements you will need the following to
make use of Build Kit:

- drush - http://drupal.org/project/drush
- drush make - http://drupal.org/project/drush_make
- git - http://git-scm.com


Getting started
---------------
Download all contrib for site:
	$ drush make --contrib-destination=./sites/all custom/scripts/themeski.make