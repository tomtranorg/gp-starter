<?php

/**
 * @file
 * Contains Batch API callbacks used during installation.
 */


/**
 * BatchAPI callback.
 *
 * @see _themeski_enable_module()
 */
function _themeski_enable_module($module, $module_name, &$context) {
  try {
  	module_enable(array($module), FALSE);
  } catch (Exception $e) {
  	drupal_set_message('Erro tai module: '.$module);
  }
  
  $context['message'] = st('Installed %module module.', array('%module' => $module_name));
}

/**
 * BatchAPI callback.
 *
 * @see _themeski_flush_caches()
 */
function _themeski_flush_caches_cron($operation, &$context) {
  $context['message'] = t('@operation', array('@operation' => $operation));
  drupal_cron_run();
  drupal_flush_all_caches();
}

function _themeski_user($operation, &$context) {
	$context['message'] = t('Create user test');
	$objUser = array(
	  'name' => 'customer',
	  'pass' => '12345',
	  'mail' => 'customer@themeski.com',
	  'status' => 1,
	  'init' => 'customer@themeski.com',		  
	);
	
	$objUser['roles'] = array(2 => t('authenticated user'));
	try {
		$account = user_save(null, $objUser);
	} catch (Exception $e) {}
}