<?php

/**
 * Override or insert variables into the maintenance page template.
 */
function themeski_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  
  if (variable_get('install_task') != 'done') {
    $footer_markup =  '<div class="message">' . t('Proudly built by') . '</div>';
    $footer_markup .=  '<div class="logo">' . t('<a href="@url">Themeski</a>', array('@url' => '#')) . '</div>';
    $vars['footer'] = array(
      '#prefix' => '<div id="credit" class="clearfix">',
      '#markup' => $footer_markup,
      '#suffix' => '</div>',
    );
  }
}
