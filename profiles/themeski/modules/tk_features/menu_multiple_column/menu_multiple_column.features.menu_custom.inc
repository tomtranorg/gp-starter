<?php
/**
 * @file
 * menu_multiple_column.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function menu_multiple_column_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-menu-multiple-column.
  $menus['menu-menu-multiple-column'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'title' => 'Menu multiple column',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Menu multiple column');


  return $menus;
}
