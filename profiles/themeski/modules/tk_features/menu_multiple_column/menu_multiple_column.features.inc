<?php
/**
 * @file
 * menu_multiple_column.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function menu_multiple_column_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
