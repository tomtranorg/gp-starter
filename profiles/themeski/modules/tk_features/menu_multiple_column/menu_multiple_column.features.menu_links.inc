<?php
/**
 * @file
 * menu_multiple_column.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function menu_multiple_column_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-menu-multiple-column:<front>
  $menu_links['menu-menu-multiple-column:<front>'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Menu item - 2',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com
  $menu_links['menu-menu-multiple-column:https://www.dantri.com'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com',
    'router_path' => '',
    'link_title' => 'Menu item - 2.1',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => '<front>',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com1
  $menu_links['menu-menu-multiple-column:https://www.dantri.com1'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com1',
    'router_path' => '',
    'link_title' => 'Menu item - 2.1.2',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'parent_path' => 'https://www.dantri.com',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com2
  $menu_links['menu-menu-multiple-column:https://www.dantri.com2'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com2',
    'router_path' => '',
    'link_title' => 'Menu item - 2.1.1',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'https://www.dantri.com',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com3
  $menu_links['menu-menu-multiple-column:https://www.dantri.com3'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com3',
    'router_path' => '',
    'link_title' => 'Menu item - 2.2',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 1,
    'parent_path' => '<front>',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com4
  $menu_links['menu-menu-multiple-column:https://www.dantri.com4'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com4',
    'router_path' => '',
    'link_title' => 'Menu item - 2.2.1',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'https://www.dantri.com3',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com5
  $menu_links['menu-menu-multiple-column:https://www.dantri.com5'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com5',
    'router_path' => '',
    'link_title' => 'Menu item - 2.3',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 2,
    'parent_path' => '<front>',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com55
  $menu_links['menu-menu-multiple-column:https://www.dantri.com55'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com55',
    'router_path' => '',
    'link_title' => 'Menu item - 1',
    'options' => array(
      'attributes' => array(),
      'menu_views' => array(
        'mlid' => 0,
        'type' => 'link',
        'original_path' => '',
        'view' => array(
          'name' => FALSE,
        ),
      ),
      'menu_attach_block' => array(
        'name' => '',
        'orientation' => 'horizontal',
        'mlid' => 0,
        'plid' => 0,
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com6
  $menu_links['menu-menu-multiple-column:https://www.dantri.com6'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com6',
    'router_path' => '',
    'link_title' => 'Menu item - 3',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 1,
  );
  // Exported menu link: menu-menu-multiple-column:https://www.dantri.com7
  $menu_links['menu-menu-multiple-column:https://www.dantri.com7'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.dantri.com7',
    'router_path' => '',
    'link_title' => 'Menu item - 3.1',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'https://www.dantri.com6',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.facebook.com2
  $menu_links['menu-menu-multiple-column:https://www.facebook.com2'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.facebook.com2',
    'router_path' => '',
    'link_title' => 'Menu item - 3.2',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'parent_path' => 'https://www.dantri.com6',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.facebook.com45
  $menu_links['menu-menu-multiple-column:https://www.facebook.com45'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.facebook.com45',
    'router_path' => '',
    'link_title' => 'Menu item - 2.3.1',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'https://www.dantri.com5',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.haivl.com3
  $menu_links['menu-menu-multiple-column:https://www.haivl.com3'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.haivl.com3',
    'router_path' => '',
    'link_title' => 'Menu item - 3.1.3',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'parent_path' => 'https://www.dantri.com7',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.kenh14.com5
  $menu_links['menu-menu-multiple-column:https://www.kenh14.com5'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.kenh14.com5',
    'router_path' => '',
    'link_title' => 'Menu item - 4',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
  );
  // Exported menu link: menu-menu-multiple-column:https://www.vnexpress.com1
  $menu_links['menu-menu-multiple-column:https://www.vnexpress.com1'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.vnexpress.com1',
    'router_path' => '',
    'link_title' => 'Menu item - 3.1.1',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'parent_path' => 'https://www.dantri.com7',
  );
  // Exported menu link: menu-menu-multiple-column:https://www.vnexpress.com8
  $menu_links['menu-menu-multiple-column:https://www.vnexpress.com8'] = array(
    'menu_name' => 'menu-menu-multiple-column',
    'link_path' => 'https://www.vnexpress.com8',
    'router_path' => '',
    'link_title' => 'Menu item - 2.1.3',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'parent_path' => 'https://www.dantri.com',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Menu item - 1');
  t('Menu item - 2');
  t('Menu item - 2.1');
  t('Menu item - 2.1.1');
  t('Menu item - 2.1.2');
  t('Menu item - 2.1.3');
  t('Menu item - 2.2');
  t('Menu item - 2.2.1');
  t('Menu item - 2.3');
  t('Menu item - 2.3.1');
  t('Menu item - 3');
  t('Menu item - 3.1');
  t('Menu item - 3.1.1');
  t('Menu item - 3.1.3');
  t('Menu item - 3.2');
  t('Menu item - 4');


  return $menu_links;
}
