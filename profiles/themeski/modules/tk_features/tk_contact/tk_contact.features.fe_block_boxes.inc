<?php
/**
 * @file
 * tk_contact.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function tk_contact_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Contact information';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'tk_contact_information';
  $fe_block_boxes->body = '<div class="form-item"><p>Name sample co.LTD</p><p>Add: Auguststr. 69, 10117 Berlin, Germany</p><p>Tel: +49030243459</p><p>Website: webmaster@geekpolis.com</p><iframe frameborder="0" height="350" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Berlin,+Germany&amp;aq=0&amp;oq=ber&amp;sll=38.410558,-95.712891&amp;sspn=36.92804,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Berlin,+Germany&amp;ll=52.519171,13.406091&amp;spn=0.451243,1.352692&amp;t=m&amp;z=10&amp;output=embed" width="425"></iframe></div>';

  $export['tk_contact_information'] = $fe_block_boxes;

  return $export;
}
