<?php
/**
 * @file
 * tk_twitter.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function tk_twitter_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-tweets-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'tweets-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
