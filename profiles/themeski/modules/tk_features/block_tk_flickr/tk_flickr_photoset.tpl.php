<?php
/*
 * Available vars:
 * $set: containing the thumbnail images (Output from flickrgallery_photo.tpl.php)
 * $meta: more information about the set
 */
?>
  <?php
	$i = 0;
	$len = count($photoset);
  ?>
  <?php foreach ($photoset as $key => $set) : ?>
		
	  <?php if($i == 0): ?>
		  <div class="flickr-wrap first">
			<?php print $set; ?>
		  </div>
	  <?php elseif($i == ($len -1) ): ?>
		  <div class="flickr-wrap last">
			<?php print $set; ?>
		  </div>
	  <?php else: ?>
	       <div class="flickr-wrap">
			<?php print $set; ?>
		   </div>
	  <?php endif; ?>
	  <?php $i++; ?>
  <?php endforeach; ?>
  <div class='flickrgallery-return'><?php print l(t('Back to sets'), variable_get('flickrgallery_path', 'flickr')); ?></div>