<?php
/**
 * @file
 * demo_content_basicpage.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function demo_content_basicpage_content_defaults() {
  $content = array();

  $content['page_content_1'] = (object) array(
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/page_content_1',
      'router_path' => 'node/%',
      'link_title' => 'Styleguide',
      'options' => array(
        'attributes' => array(),
        'menu_views' => array(
          'mlid' => 0,
          'type' => 'link',
          'original_path' => '',
          'view' => array(
            'name' => FALSE,
          ),
        ),
      ),
      'module' => 'menu',
      'hidden' => 0,
      'external' => 0,
      'has_children' => 1,
      'expanded' => 0,
      'weight' => -49,
      'identifier' => 'main-menu:node-name/page_content_1',
    ),
    'title' => 'Styleguide',
    'status' => 1,
    'promote' => 0,
    'sticky' => 0,
    'type' => 'page',
    'language' => 'und',
    'created' => 1379295372,
    'comment' => 1,
    'translate' => 0,
    'machine_name' => 'page_content_1',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => '<p>Vestibulum eget libero velit, at sollicitudin ipsum.</p>

<h1>H1 Title Heading</h1>

<p>Vestibulum eget libero velit, at sollicitudin ipsum. Vivamus tellus ante, lobortis eu posuere sed, bibendum eu erat.</p>

<h2>H2 Title Heading</h2>

<p>Aliquam sollicitudin felis sit amet mauris mattis consectetur. Praesent sed bibendum est.</p>

<h3>H3 Title Heading</h3>

<p>Morbi quis arcu et nisl varius adipiscing sed sit amet orci. Duis non erat turpis.</p>

<h4>H4 Title Heading</h4>

<p>Vestibulum sed scelerisque eros. Nullam ipsum sapien, elementum a.</p>

<h3>Links Styling</h3>

<h2><a href="#" title="H2 Link Hover">H2 Link</a></h2>

<h3><a href="#" title="H3 Link Hover">H3 Link</a></h3>

<h4><a href="#" title="h3 Link Hover">H4 Link</a></h4>

<p><a href="#">This is a normal link</a></p>

<h2>Pager</h2>

<div class="item-list">
<ul class="pager">
	<li class="pager-first even first"><a class="active" href="#" title="Go to first page">« first</a></li>
	<li class="pager-previous odd"><a class="active" href="#" title="Go to previous page">‹ previous</a></li>
	<li class="pager-item odd"><a class="active" href="#" title="Go to page 1">1</a></li>
	<li class="pager-item even"><a class="active" href="#" title="Go to page 2">2</a></li>
	<li class="pager-current even first">3</li>
	<li class="pager-item odd"><a class="active" href="#" title="Go to page 4">4</a></li>
	<li class="pager-item even"><a class="active" href="#" title="Go to page 5">5</a></li>
	<li class="pager-next odd"><a class="active" href="#" title="Go to next page">next ›</a></li>
	<li class="pager-last even last"><a class="active" href="#" title="Go to last page">last »</a></li>
</ul>
</div>

<h2>Table</h2>

<table>
	<thead>
		<tr>
			<th>Table</th>
			<th>Header 1</th>
			<th><a class="active" href="#" title="sort by Type">Link in Header</a></th>
			<th class="active"><a class="active" href="#" title="sort by Updated">Sortable<img alt="sort ascending" height="13" src="misc/arrow-asc.png" title="sort ascending" typeof="foaf:Image" width="13" /></a></th>
		</tr>
	</thead>
	<tbody>
		<tr class="even">
			<td>Row 1</td>
			<td>Division</td>
			<td>Division</td>
			<td>Division</td>
		</tr>
		<tr class="odd">
			<td>Row 2</td>
			<td>Division</td>
			<td>Division</td>
			<td>Division</td>
		</tr>
		<tr class="even">
			<td>Row 3</td>
			<td>Division</td>
			<td>Division</td>
			<td>Division</td>
		</tr>
		<tr class="odd">
			<td>Row 4</td>
			<td>Division</td>
			<td>Division</td>
			<td>Division</td>
		</tr>
		<tr class="even">
			<td>Row 5</td>
			<td>Division</td>
			<td>Division</td>
			<td>Division</td>
		</tr>
	</tbody>
</table>

<h2>BlockQuote</h2>

<blockquote>
<p>Abundant well impotent trustful far a quail unanimous concentric overslept after this then meagerly gave impala where serenely richly broadcast but more.</p>
</blockquote>

<p>Porttitor ut sit amet enim. <a href="#">Pellentesque cursus faucibus dapibus</a>. Proin gravida euismod turpis non fringilla. Aenean auctor lacus est.</p>

<ul>
	<li><a href="#">Ut eleifend feugiat nisl</a></li>
	<li><a href="#">Nam nibh libero, fermentum ut varius ac</a></li>
	<li>Etiam viverra auctor malesuada.</li>
	<li>Quisque mi risus, consequat id iaculis in</li>
</ul>

<p>Maecenas ac urna massa. <a href="#">Proin feugiat mauris vitae quam aliquam</a> a aliquet dui volutpat. Quisque mi risus, consequat id iaculis in, feugiat vel eros. Sed semper arcu ac mauris fringilla accumsan egestas lorem posuere.</p>

<h2>Image style</h2>

<p><img src="sites/default/files/pictures/image-style.jpg" /><a href="#"><img src="sites/default/files/pictures/image-link.jpg" style="margin-left: 20px;" /></a></p>
',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p>Vestibulum eget libero velit, at sollicitudin ipsum.</p>
<h1>H1 Title Heading</h1>
<p>Vestibulum eget libero velit, at sollicitudin ipsum. Vivamus tellus ante, lobortis eu posuere sed, bibendum eu erat.</p>
<h2>H2 Title Heading</h2>
<p>Aliquam sollicitudin felis sit amet mauris mattis consectetur. Praesent sed bibendum est.</p>
<h3>H3 Title Heading</h3>
<p>Morbi quis arcu et nisl varius adipiscing sed sit amet orci. Duis non erat turpis.</p>
<h4>H4 Title Heading</h4>
<p>Vestibulum sed scelerisque eros. Nullam ipsum sapien, elementum a.</p>
<h3>Links Styling</h3>
<h2><a href="#" title="H2 Link Hover">H2 Link</a></h2>
<h3><a href="#" title="H3 Link Hover">H3 Link</a></h3>
<h4><a href="#" title="h3 Link Hover">H4 Link</a></h4>
<p><a href="#">This is a normal link</a></p>
<h2>Pager</h2>
<div class="item-list">
<ul class="pager"><li class="pager-first even first"><a class="active" href="#" title="Go to first page">« first</a></li>
<li class="pager-previous odd"><a class="active" href="#" title="Go to previous page">‹ previous</a></li>
<li class="pager-item odd"><a class="active" href="#" title="Go to page 1">1</a></li>
<li class="pager-item even"><a class="active" href="#" title="Go to page 2">2</a></li>
<li class="pager-current even first">3</li>
<li class="pager-item odd"><a class="active" href="#" title="Go to page 4">4</a></li>
<li class="pager-item even"><a class="active" href="#" title="Go to page 5">5</a></li>
<li class="pager-next odd"><a class="active" href="#" title="Go to next page">next ›</a></li>
<li class="pager-last even last"><a class="active" href="#" title="Go to last page">last »</a></li>
</ul></div>
<h2>Table</h2>
<table><thead><tr><th>Table</th>
<th>Header 1</th>
<th><a class="active" href="#" title="sort by Type">Link in Header</a></th>
<th class="active"><a class="active" href="#" title="sort by Updated">Sortable<img alt="sort ascending" height="13" src="misc/arrow-asc.png" title="sort ascending" typeof="foaf:Image" width="13" /></a></th>
</tr></thead><tbody><tr class="even"><td>Row 1</td>
<td>Division</td>
<td>Division</td>
<td>Division</td>
</tr><tr class="odd"><td>Row 2</td>
<td>Division</td>
<td>Division</td>
<td>Division</td>
</tr><tr class="even"><td>Row 3</td>
<td>Division</td>
<td>Division</td>
<td>Division</td>
</tr><tr class="odd"><td>Row 4</td>
<td>Division</td>
<td>Division</td>
<td>Division</td>
</tr><tr class="even"><td>Row 5</td>
<td>Division</td>
<td>Division</td>
<td>Division</td>
</tr></tbody></table><h2>BlockQuote</h2>
<blockquote><p>Abundant well impotent trustful far a quail unanimous concentric overslept after this then meagerly gave impala where serenely richly broadcast but more.</p>
</blockquote>
<p>Porttitor ut sit amet enim. <a href="#">Pellentesque cursus faucibus dapibus</a>. Proin gravida euismod turpis non fringilla. Aenean auctor lacus est.</p>
<ul><li><a href="#">Ut eleifend feugiat nisl</a></li>
<li><a href="#">Nam nibh libero, fermentum ut varius ac</a></li>
<li>Etiam viverra auctor malesuada.</li>
<li>Quisque mi risus, consequat id iaculis in</li>
</ul><p>Maecenas ac urna massa. <a href="#">Proin feugiat mauris vitae quam aliquam</a> a aliquet dui volutpat. Quisque mi risus, consequat id iaculis in, feugiat vel eros. Sed semper arcu ac mauris fringilla accumsan egestas lorem posuere.</p>
<h2>Image style</h2>
<p><img src="sites/default/files/pictures/image-style.jpg" /><a href="#"><img src="sites/default/files/pictures/image-link.jpg" style="margin-left: 20px;" /></a></p>
',
          'safe_summary' => '',
        ),
      ),
    ),
  );

  $content['page_content_2'] = (object) array(
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/page_content_2',
      'router_path' => 'node/%',
      'link_title' => 'Layout',
      'options' => array(
        'attributes' => array(),
        'menu_views' => array(
          'mlid' => 0,
          'type' => 'link',
          'original_path' => 'node/22',
          'view' => array(
            'name' => FALSE,
          ),
        ),
      ),
      'module' => 'menu',
      'hidden' => 0,
      'external' => 0,
      'has_children' => 1,
      'expanded' => 0,
      'weight' => -47,
      'identifier' => 'main-menu:node-name/page_content_2',
    ),
    'title' => 'No sidebar',
    'status' => 1,
    'promote' => 0,
    'sticky' => 0,
    'type' => 'page',
    'language' => 'und',
    'created' => 1379296795,
    'comment' => 1,
    'translate' => 0,
    'machine_name' => 'page_content_2',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => '<p>Vivamus porttitor eu libero nec venenatis. In non velit semper, convallis turpis non, euismod magna. Mauris rhoncus faucibus elit quis lacinia. Aenean ut lacinia est. Morbi velit nulla, aliquam eget augue et, ullamcorper elementum nulla. Aliquam in mollis enim. Nam adipiscing enim vitae diam aliquam faucibus. Pellentesque id accumsan arcu. Duis at diam justo. Duis ut felis ac orci rutrum blandit. Maecenas eros turpis, faucibus quis scelerisque nec, viverra non mi. Vivamus arcu ante, porttitor nec enim at, auctor semper sem. Nullam aliquet ac quam eget egestas! Phasellus in gravida neque; vel egestas turpis. Donec tincidunt hendrerit placerat. Suspendisse at orci in sapien tristique malesuada sed et sem. Fusce congue eget erat venenatis pretium. Nullam sodales leo diam, ut consequat tellus gravida at. Phasellus sed ultrices nibh! Nunc feugiat interdum lacinia. Maecenas pharetra ac mi ac aliquam. Vivamus a dolor vel neque rhoncus pretium. Cras porttitor placerat congue. Pellentesque pellentesque; turpis at egestas facilisis, orci risus condimentum dui, at ultrices urna quam sed nisl! Nunc viverra erat nisl, sed vestibulum nisi vehicula eu. Proin aliquam urna eu orci pulvinar, in metus.</p>
',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p>Vivamus porttitor eu libero nec venenatis. In non velit semper, convallis turpis non, euismod magna. Mauris rhoncus faucibus elit quis lacinia. Aenean ut lacinia est. Morbi velit nulla, aliquam eget augue et, ullamcorper elementum nulla. Aliquam in mollis enim. Nam adipiscing enim vitae diam aliquam faucibus. Pellentesque id accumsan arcu. Duis at diam justo. Duis ut felis ac orci rutrum blandit. Maecenas eros turpis, faucibus quis scelerisque nec, viverra non mi. Vivamus arcu ante, porttitor nec enim at, auctor semper sem. Nullam aliquet ac quam eget egestas! Phasellus in gravida neque; vel egestas turpis. Donec tincidunt hendrerit placerat. Suspendisse at orci in sapien tristique malesuada sed et sem. Fusce congue eget erat venenatis pretium. Nullam sodales leo diam, ut consequat tellus gravida at. Phasellus sed ultrices nibh! Nunc feugiat interdum lacinia. Maecenas pharetra ac mi ac aliquam. Vivamus a dolor vel neque rhoncus pretium. Cras porttitor placerat congue. Pellentesque pellentesque; turpis at egestas facilisis, orci risus condimentum dui, at ultrices urna quam sed nisl! Nunc viverra erat nisl, sed vestibulum nisi vehicula eu. Proin aliquam urna eu orci pulvinar, in metus.</p>
',
          'safe_summary' => '',
        ),
      ),
    ),
  );

  $content['page_content_3'] = (object) array(
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/page_content_3',
      'router_path' => 'node/%',
      'link_title' => 'One sidebar',
      'options' => array(
        'attributes' => array(),
        'menu_views' => array(
          'mlid' => 0,
          'type' => 'link',
          'original_path' => '',
          'view' => array(
            'name' => FALSE,
          ),
        ),
      ),
      'module' => 'menu',
      'hidden' => 0,
      'external' => 0,
      'has_children' => 0,
      'expanded' => 0,
      'weight' => 0,
      'parent_path' => 'node-name/page_content_2',
      'identifier' => 'main-menu:node-name/page_content_3',
    ),
    'title' => 'One sidebar',
    'status' => 1,
    'promote' => 0,
    'sticky' => 0,
    'type' => 'page',
    'language' => 'und',
    'created' => 1379296938,
    'comment' => 1,
    'translate' => 0,
    'machine_name' => 'page_content_3',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => 'Vestibulum porta ut mauris at malesuada. Etiam ut vestibulum nunc. Nulla convallis sit amet diam at sodales. Praesent tellus quam; egestas sit amet nisl ut, cursus iaculis metus. Morbi justo velit, facilisis sed congue nec, bibendum non urna. Quisque urna ante, pretium ac ultricies id, commodo eu diam. Nunc vehicula odio ut facilisis congue. Duis gravida ac purus eget tempus? Aenean id felis arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer aliquet urna in tellus elementum fringilla. Suspendisse pretium in purus eget pharetra. Maecenas eu dui augue. Aenean mollis leo dolor, vitae egestas erat varius at.

Phasellus ullamcorper, lacus eu tincidunt scelerisque, tellus neque adipiscing ligula, ac malesuada mi dui ut mi. Suspendisse nisl justo, rutrum ut fermentum sit amet, adipiscing at purus. Phasellus lacus felis; fringilla sed consequat ut, pulvinar ac magna. Nam luctus augue a nulla gravida facilisis. Etiam ac odio eu neque gravida vestibulum. Pellentesque ullamcorper accumsan rhoncus. Etiam ultricies nunc vitae nibh eleifend imperdiet. Vestibulum malesuada elementum sem, blandit viverra diam venenatis eget. Quisque fringilla arcu eget blandit sodales? Nullam id.
',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p>Vestibulum porta ut mauris at malesuada. Etiam ut vestibulum nunc. Nulla convallis sit amet diam at sodales. Praesent tellus quam; egestas sit amet nisl ut, cursus iaculis metus. Morbi justo velit, facilisis sed congue nec, bibendum non urna. Quisque urna ante, pretium ac ultricies id, commodo eu diam. Nunc vehicula odio ut facilisis congue. Duis gravida ac purus eget tempus? Aenean id felis arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer aliquet urna in tellus elementum fringilla. Suspendisse pretium in purus eget pharetra. Maecenas eu dui augue. Aenean mollis leo dolor, vitae egestas erat varius at.</p>
<p>Phasellus ullamcorper, lacus eu tincidunt scelerisque, tellus neque adipiscing ligula, ac malesuada mi dui ut mi. Suspendisse nisl justo, rutrum ut fermentum sit amet, adipiscing at purus. Phasellus lacus felis; fringilla sed consequat ut, pulvinar ac magna. Nam luctus augue a nulla gravida facilisis. Etiam ac odio eu neque gravida vestibulum. Pellentesque ullamcorper accumsan rhoncus. Etiam ultricies nunc vitae nibh eleifend imperdiet. Vestibulum malesuada elementum sem, blandit viverra diam venenatis eget. Quisque fringilla arcu eget blandit sodales? Nullam id.</p>
',
          'safe_summary' => '',
        ),
      ),
    ),
  );

  $content['page_content_4'] = (object) array(
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/page_content_4',
      'router_path' => 'node/%',
      'link_title' => 'Two sidebar',
      'options' => array(
        'attributes' => array(),
        'menu_views' => array(
          'mlid' => 0,
          'type' => 'link',
          'original_path' => '',
          'view' => array(
            'name' => FALSE,
          ),
        ),
      ),
      'module' => 'menu',
      'hidden' => 0,
      'external' => 0,
      'has_children' => 0,
      'expanded' => 0,
      'weight' => 0,
      'parent_path' => 'node-name/page_content_2',
      'identifier' => 'main-menu:node-name/page_content_4',
    ),
    'title' => 'Two sidebar',
    'status' => 1,
    'promote' => 0,
    'sticky' => 0,
    'type' => 'page',
    'language' => 'und',
    'created' => 1379296975,
    'comment' => 1,
    'translate' => 0,
    'machine_name' => 'page_content_4',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => 'Donec id fermentum eros. Pellentesque convallis dui eget tincidunt lobortis. Vivamus at imperdiet arcu, a rutrum neque. Praesent dignissim vulputate odio eu scelerisque! Sed diam nulla, vehicula non sem id, tempor facilisis dui. Nullam auctor enim nibh. Nulla facilisi. Nunc tristique pharetra ipsum, nec tincidunt leo. Integer ac ullamcorper sapien.

Quisque vel ipsum purus. Donec pharetra augue non dictum pretium! Nunc nec facilisis orci. Sed bibendum, est eu eleifend rutrum, turpis nisi scelerisque libero, nec lacinia enim dolor eu metus. Aliquam erat volutpat. Vestibulum dapibus iaculis adipiscing. Donec at cursus nulla; nec fermentum ligula. Duis placerat, neque sed euismod vulputate, sem magna varius libero, a pretium urna diam et arcu!

Donec pretium pharetra tristique. Proin nibh orci; malesuada vitae pellentesque eu; laoreet et ante. Maecenas eget tincidunt velit. Morbi libero magna, feugiat vel fringilla sit amet, tristique vitae odio. In id est nisl. Nunc sit amet blandit tortor. Vivamus lobortis lacus vel eros ullamcorper iaculis eu non nisl. Maecenas sodales accumsan consectetur. Suspendisse sollicitudin ligula id est ornare, ac volutpat eros sollicitudin. Morbi turpis duis.
',
          'summary' => '',
          'format' => 'full_html',
          'safe_value' => '<p>Donec id fermentum eros. Pellentesque convallis dui eget tincidunt lobortis. Vivamus at imperdiet arcu, a rutrum neque. Praesent dignissim vulputate odio eu scelerisque! Sed diam nulla, vehicula non sem id, tempor facilisis dui. Nullam auctor enim nibh. Nulla facilisi. Nunc tristique pharetra ipsum, nec tincidunt leo. Integer ac ullamcorper sapien.</p>
<p>Quisque vel ipsum purus. Donec pharetra augue non dictum pretium! Nunc nec facilisis orci. Sed bibendum, est eu eleifend rutrum, turpis nisi scelerisque libero, nec lacinia enim dolor eu metus. Aliquam erat volutpat. Vestibulum dapibus iaculis adipiscing. Donec at cursus nulla; nec fermentum ligula. Duis placerat, neque sed euismod vulputate, sem magna varius libero, a pretium urna diam et arcu!</p>
<p>Donec pretium pharetra tristique. Proin nibh orci; malesuada vitae pellentesque eu; laoreet et ante. Maecenas eget tincidunt velit. Morbi libero magna, feugiat vel fringilla sit amet, tristique vitae odio. In id est nisl. Nunc sit amet blandit tortor. Vivamus lobortis lacus vel eros ullamcorper iaculis eu non nisl. Maecenas sodales accumsan consectetur. Suspendisse sollicitudin ligula id est ornare, ac volutpat eros sollicitudin. Morbi turpis duis.</p>
',
          'safe_summary' => '',
        ),
      ),
    ),
  );

return $content;
}
