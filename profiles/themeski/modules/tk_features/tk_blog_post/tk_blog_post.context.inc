<?php
/**
 * @file
 * tk_blog_post.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function tk_blog_post_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_post';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog_post' => 'blog_post',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'blog-post' => 'blog-post',
        'archive/*' => 'archive/*',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'blog_categories' => 'blog_categories',
        'tags' => 'tags',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
    'views' => array(
      'values' => array(
        'blog_post:page' => 'blog_post:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_category-block' => array(
          'module' => 'views',
          'delta' => 'blog_category-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-blog_post-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_post-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'tagclouds-1' => array(
          'module' => 'tagclouds',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['blog_post'] = $context;

  return $export;
}
