<?php
/**
 * @file
 * tk_portfolio.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function tk_portfolio_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'categories_portfolio';
  $ds_field->label = 'Categories';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?php
$views_portfolio = views_get_view(\'portfolio\');
$path_page_portfolio = $views_portfolio->display[\'page\']->display_options[\'path\'];
$categories = array();
if(!empty($entity->field_categories[\'und\'])){
    foreach($entity->field_categories[\'und\'] as $value){
        $term = taxonomy_term_load($value[\'tid\']);
        $categories[] = l($term->name,$path_page_portfolio,array(\'query\'=>array($views_portfolio->display[\'page\']->display_options[\'filters\'][\'field_categories_tid\'][\'expose\'][\'identifier\']=>$value[\'tid\'])));
   }
}
print implode(\',\',$categories);
?>',
      'format' => 'ds_code',
    ),
    'use_token' => 0,
  );
  $export['categories_portfolio'] = $ds_field;

  return $export;
}
