<?php
/**
 * @file
 * tk_footer.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function tk_footer_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_menu_copyright';
  $fe_block_boxes->body = '<div class="clearfix"><ul class="menu-footer inline"><li><a href="#">Home</a></li><li><a href="#">StyleGuide</a></li><li><a href="#">Blog</a></li><li><a href="#">Layout</a></li><li><a href="#">Contact</a></li></ul></div><div class="copyright">&copy; 2013 Cognition&nbsp;<a href="http://themeski.com" target="_blank">Drupal Theme</a>&nbsp;designed by Geekpolis.com</div>';

  $export['footer_menu_copyright'] = $fe_block_boxes;

  return $export;
}
